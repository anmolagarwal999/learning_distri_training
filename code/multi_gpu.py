#!/usr/bin/env python

# !conda env list
# # In[2]:


# get_ipython().system('ls')


# # In[3]:


import torch
import torch.nn.functional as F
from torch.utils.data import Dataset, DataLoader
import importlib
import datetime

# ### DDP specific modules
import torch.multiprocessing as mp # wrapper around python's multiprocessing library

# module which we will use to take in the input data and distribute it across all the GPUs
from torch.utils.data.distributed import DistributedSampler

# main module
from torch.nn.parallel import DistributedDataParallel as DDP

# initialize and destroy our distributed process group
from torch.distributed import init_process_group, destroy_process_group
import os

import distri_utils.datautils as datautils

importlib.reload(datautils)
########################################
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    arr = [ OKCYAN, OKGREEN, ENDC, BOLD, UNDERLINE, WARNING, OKBLUE]
####################################

# The distributed process group contains all the processes which are running on the different GPUs. Typically each GPU runs one process. Setting a group is necessary so that all processes can discover and communicate with each other.
def ddp_setup(rank, world_size):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    print(f"Inside ddp_setup for {rank}/{world_size}")
    os.environ["MASTER_ADDR"] = "localhost"
    os.environ["MASTER_PORT"] = "12357"
    
    # initialize the process group
    # this is a blocking call, which will wait for all the processes (ie ranked processes)
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    print("init_process_group done")
    #TODO: revisit
    torch.cuda.set_device(rank)

#######################################

class Trainer:
    def __init__(
        self,
        model: torch.nn.Module,
        train_data: DataLoader,
        optimizer: torch.optim.Optimizer,
        gpu_id: int,
        save_every: int, 
    ) -> None:
        self.gpu_id = gpu_id
        self.model = model.to(gpu_id)
        self.train_data = train_data
        self.optimizer = optimizer
        self.save_every = save_every
        
        
        '''
        1) For single-device modules, device_ids can contain exactly one device id, 
            which represents the only CUDA device where the input module corresponding
            to this process resides. 
            Alternatively, device_ids can also be None. 
            
        2) For multi-device modules and CPU modules, device_ids must be None.
        When device_ids is None for both cases, 
        both the input data for the forward pass and the actual module must
        be placed on the correct device. (default: None)
        
        
        '''
        
        self.init_color()
        print("Inside trainer init")
        self.model = DDP(self.model, device_ids = [self.gpu_id])
        
        print("Trainer init done")
    
    def init_color(self):
        print(bcolors.arr[self.gpu_id])
        # print("GPU id:", self.gpu_id)

    def _run_batch(self, source, targets):
        self.optimizer.zero_grad()
        output = self.model(source)
        #loss = F.cross_entropy(output, targets)
        loss_fn = torch.nn.MSELoss()
        loss = loss_fn(output, targets)
        print(loss)
        loss.backward()
        self.optimizer.step()
        print(bcolors.arr[self.gpu_id],"Optimizer stepped")
        
        

    def _run_epoch(self, epoch):
        b_sz = len(next(iter(self.train_data))[0])
        print(bcolors.arr[self.gpu_id], f"[GPU{self.gpu_id}] Epoch {epoch} | Batchsize: {b_sz} | Steps: {len(self.train_data)}")
        
        self.train_data.sampler.set_epoch(epoch)
        for source, targets in self.train_data:
            print(bcolors.arr[self.gpu_id],"Source in this batch is:", source)
            source = source.to(self.gpu_id)
            targets = targets.to(self.gpu_id)
            self._run_batch(source, targets)
        print(bcolors.arr[self.gpu_id],"Epoch over Summary")
        if self.gpu_id == 0:
            for idx, param in enumerate(self.model.parameters()):
                print("idx: ", idx, param.data)
        print(bcolors.arr[self.gpu_id],"#################")

    def _save_checkpoint(self, epoch):
        
        # Wrapping in DDPG changes the way the parameters are accessed
        ckp = self.model.module.state_dict()
        PATH = f"checkpoint_{self.gpu_id}.pt"
        torch.save(ckp, PATH)
        print(bcolors.arr[self.gpu_id],f"Epoch {epoch} | Training checkpoint saved at {PATH}")

    def train(self, max_epochs: int):
        for epoch in range(max_epochs):
            self._run_epoch(epoch)
            if epoch % self.save_every == 0:
                self._save_checkpoint(epoch)


def load_train_objs():
    NUM_DATA_POINTS = 1000
    NUM_INPUT_FEATURES = 1
    train_set = datautils.MyTrainDataset(NUM_DATA_POINTS, NUM_INPUT_FEATURES)  # load your dataset
    model = torch.nn.Linear(NUM_INPUT_FEATURES, 1)  # load your model
    # optimizer = torch.optim.SGD(model.parameters(), lr=1e-5)
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-5)
    return train_set, model, optimizer


def prepare_dataloader(dataset: Dataset, batch_size: int):
    return DataLoader(
        dataset,
        batch_size=batch_size,
        pin_memory=True,
        shuffle=False, 
        sampler=DistributedSampler(dataset)
    )



def main_proc(curr_rank, world_size, save_every, BATCH_SIZE, total_epochs):
    print("Curr rank received is: ", curr_rank)
    ddp_setup(curr_rank, world_size)
    dataset, model, optimizer = load_train_objs()
    train_data = prepare_dataloader(dataset, BATCH_SIZE)
    trainer = Trainer(model, train_data, optimizer, curr_rank, save_every)
    trainer.train(total_epochs)
    destroy_process_group()


_world_size = torch.cuda.device_count()
print("Actual number of cuda devices is: ", _world_size)
_world_size = 1

_BATCH_SIZE = 64//_world_size
_save_every = 10
_total_epochs = 10000


# In[12]:

start_time = datetime.datetime.now()
if __name__ == "__main__":
    print("Starting")
    print("World size is: ", _world_size)

    mp.spawn(main_proc, 
             args=(_world_size, _save_every, _BATCH_SIZE, _total_epochs), 
             nprocs=_world_size)
end_time = datetime.datetime.now()

print("Total time elapsed: ", end_time-start_time)