import torch
from torch.utils.data import Dataset
import datetime


class MyTrainDataset(Dataset):
    def __init__(self, num_data_points, num_features=1):
        print(f"Creating {num_data_points} data points")
        
        self.gt_weights = torch.ones(num_features)
        self.gt_bias = torch.ones(1)
        
        # print("Ground truth weights are: ", self.gt_weights, self.gt_bias)
        
        self.num_data_points = num_data_points
        # self.data = [[torch.rand(num_features),None] for _ in range(num_data_points)]
        # self.data = [[(torch.ones(num_features)*_num) - num_data_points//2,None] for _num in range(num_data_points)]
        self.data = [[torch.rand(num_features),None] for _num in range(num_data_points)]
        
        for idx, (curr_in, curr_out) in enumerate(self.data):
            self.data[idx][1] = self.gt_weights@curr_in+self.gt_bias
            
        self.data = [tuple(_x) for _x in self.data]
        # print("Data is: ", *self.data, sep="\n")

    def __len__(self):
        return self.num_data_points
    
    def __getitem__(self, index):
        return self.data[index]
    
# class MyTrainDataset(Dataset):
#     def __init__(self, size):
#         self.size = size
#         self.data = [(torch.rand(20), torch.rand(1)) for _ in range(size)]

#     def __len__(self):
#         return self.size
    
#     def __getitem__(self, index):
#         return self.data[index]